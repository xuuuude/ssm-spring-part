package com.atguigu.advice;

import org.aspectj.lang.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Order(1)
public class LogAdvice {
    @Before("com.atguigu.pointcut.MyPointCut.point_cut()")
    public void start(){
        System.out.println("方法开始了");
    }

    @After("com.atguigu.pointcut.MyPointCut.point_cut()")
    public void after(){
        System.out.println("方法结束了");
    }

    @AfterThrowing("com.atguigu.pointcut.MyPointCut.point_cut()")
    public void error(){
        System.out.println("方法出错了");
    }

}
