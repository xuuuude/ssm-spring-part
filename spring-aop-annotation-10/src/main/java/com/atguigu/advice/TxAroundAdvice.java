package com.atguigu.advice;

import org.apiguardian.api.API;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class TxAroundAdvice {


    @Around("com.atguigu.pointcut.MyPointCut.point_cut()")
    public Object aroundTransaction(ProceedingJoinPoint proceedingJoinPoint)  {
        Object[] args = proceedingJoinPoint.getArgs();
        Object result = null;


        try {
            System.out.println("开启事务");
            result = proceedingJoinPoint.proceed(args);
            System.out.println("结束事务");
        } catch (Throwable e) {
            System.out.println("事务回滚");
            throw new RuntimeException(e);
        }
        return result;
    }

}
