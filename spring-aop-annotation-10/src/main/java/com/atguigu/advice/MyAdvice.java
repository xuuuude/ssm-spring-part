package com.atguigu.advice;

import org.aspectj.lang.JoinPoint;

import org.aspectj.lang.annotation.*;

import java.lang.reflect.Modifier;

/**
 * 定义四个增强方法，获取目标方法的信息，返回值，异常对象
 * public int com..jj.*(..)
 *
 *
 *
 *
 *
 *
 *
 */

@Aspect
public class MyAdvice {

    @Before("com.atguigu.pointcut.MyPointCut.myPc()")
    public void before(JoinPoint joinPoint){
        //获取方法所在类的信息
        String className = joinPoint.getTarget().getClass().getSimpleName();
        //获取方法名
        int modifiers = joinPoint.getSignature().getModifiers();
        String string = Modifier.toString(modifiers);
        //获取目标方法的参数
        Object[] args = joinPoint.getArgs();

    }

    @AfterReturning(value = "com.atguigu.pointcut.MyPointCut.myPc()",returning = "result")
    public void afterReturning(JoinPoint joinPoint,Object result){

    }

    @After("com.atguigu.pointcut.MyPointCut.myPc()")
    public void after(JoinPoint joinPoint){

    }

    @AfterThrowing(value = "com.atguigu.pointcut.MyPointCut.myPc()",throwing ="throwable")
    public void afterThrowing(JoinPoint joinPoint,Throwable throwable){

    }





}
