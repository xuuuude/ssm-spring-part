package com.atguigu.advice;

import org.aspectj.lang.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 使用普通方式进行事务的添加
 */

@Component
@Aspect
@Order(0)
public class TxAdvice {

    @Before("com.atguigu.pointcut.MyPointCut.point_cut()")
    public void begin(){
        System.out.println("事务开启");
    }

    @AfterReturning("com.atguigu.pointcut.MyPointCut.point_cut()")
    public void commit(){
        System.out.println("事务提交");
    }

    @AfterThrowing("com.atguigu.pointcut.MyPointCut.point_cut()")
    public void rollback(){
        System.out.println("事务回滚");
    }

}
