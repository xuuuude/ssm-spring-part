package com.atguigu.pointcut;

import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * 本类用于维护切点列表
 */
@Component
public class MyPointCut {
    @Pointcut("execution(* com.atguigu.service.impl.*.*(..))")
    public void point_cut(){

    }



    @Pointcut("execution(* com.atguigu.service.impl.*.*(..))")
    public void myPc(){

    }
}
