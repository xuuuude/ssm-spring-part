package com.atguigu.test;

import com.atguigu.ioc_03.A;
import com.atguigu.ioc_03.HappyComponent;
import com.atguigu.ioc_04.JavaBean2;
import com.atguigu.ioc_05.JavaBean;
import com.atguigu.ioc_05.JavaBeanFactoryBean;
import com.atguigu.ioc_05_test.Note;
import com.atguigu.ioc_05_test.NoteFactory;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringIoCTest {

    /**
     * 如何创建一个 ioc 容器，并且能够读取配置文件
     */
    @Test
    public void createIoC(){

        // 1.直接创建容器，并且指定配置文件
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-03.xml");
        // 2.先创建 ioc 容器对象，再指定配置文件
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext();
        classPathXmlApplicationContext.setConfigLocations("spring-03.xml");
        classPathXmlApplicationContext.refresh();

        }

        @Test
    public void getBeanFromIoC(){
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext();
        classPathXmlApplicationContext.setConfigLocations("spring-03.xml");
        classPathXmlApplicationContext.refresh();

        HappyComponent happyComponent = (HappyComponent) classPathXmlApplicationContext.getBean("happyComponent");


        HappyComponent happyComponent1 = classPathXmlApplicationContext.getBean("happyComponent", HappyComponent.class);



        A happyComponent2 = classPathXmlApplicationContext.getBean(A.class);

        happyComponent2.doWork();

        System.out.println(happyComponent2 == happyComponent);

    }

    /**
     * 测试 ioc 配置和销毁方法的触发
     */
    @Test
    public void test_04(){
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring-04.xml");


        JavaBean2 bean = classPathXmlApplicationContext.getBean(JavaBean2.class);
        JavaBean2 bean1 = classPathXmlApplicationContext.getBean(JavaBean2.class);
        System.out.println(bean1 == bean);

        classPathXmlApplicationContext.close();

    }





    @Test
    public void test_05(){
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring-05.xml");
        JavaBean javaBean = classPathXmlApplicationContext.getBean("javaBean", JavaBean.class);
        System.out.println("javaBean = "+javaBean);
        // FactoryBean 工厂也会加入到 ioc 容器中，其默认名字是 &#{id}
        Object bean = classPathXmlApplicationContext.getBean("&javaBean");
        System.out.println(bean);
        classPathXmlApplicationContext.close();
    }






    @Test
    public void test_05_test(){
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring-05-test.xml");
        Note note = classPathXmlApplicationContext.getBean("note", Note.class);
        System.out.println("note = "+note);
        // FactoryBean 工厂也会加入到 ioc 容器中，其默认名字是 &#{id}
        Object bean = classPathXmlApplicationContext.getBean("&note");
        System.out.println(bean);
        classPathXmlApplicationContext.close();
    }


}
