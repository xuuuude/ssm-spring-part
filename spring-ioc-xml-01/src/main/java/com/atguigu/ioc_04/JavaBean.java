package com.atguigu.ioc_04;

public class JavaBean {
    public void init(){
        System.out.println("Java Bean init");
    }

    public void end(){
        System.out.println("Java Bean Destroyed");
    }

}
