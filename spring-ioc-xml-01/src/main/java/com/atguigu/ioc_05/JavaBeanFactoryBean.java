package com.atguigu.ioc_05;

import org.springframework.beans.factory.FactoryBean;

/**
 * 制造 JavaBean 的工厂 Bean 对象
 * 也就是说，本类是也是一个 bean 对象，本类是制造a这个bean的工厂bean
 */



public class JavaBeanFactoryBean implements FactoryBean<JavaBean> {

    private String value;

    //在外面绑定一个参数 value ，value 的另一端绑定到工厂类上，然后在对应的 xml 中对这个参数进行传值，利用 property 标签
    public void setValue(String value){
        this.value = value;
    }


    @Override
    public JavaBean getObject() throws Exception {
        //自行实现对象实例化的逻辑
        JavaBean javaBean = new JavaBean();
        //通过在 getObject 方法中对这个对象进行 set 方法传参
        javaBean.setName(value);
        return javaBean;
    }

    @Override
    public Class<?> getObjectType() {
        return JavaBean.class;
    }
}
