package com.atguigu.ioc_05_test;

import org.springframework.beans.factory.FactoryBean;

public class NoteFactory implements FactoryBean<Note> {

    @Override
    public Note getObject() throws Exception {
        Note note = new Note();
        return note;
    }

    @Override
    public Class<?> getObjectType() {
        return Note.class;
    }
}
