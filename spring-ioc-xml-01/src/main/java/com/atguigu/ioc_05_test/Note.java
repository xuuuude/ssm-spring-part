package com.atguigu.ioc_05_test;

public class Note {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
