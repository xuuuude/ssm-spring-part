package com.atguigu.test;

import com.atguigu.config.JavaConfiguration;
import com.atguigu.controller.StudentController;
import com.atguigu.pojo.Student;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringIoCTest {


    @Test
    public void test(){
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(JavaConfiguration.class);

        StudentController bean = annotationConfigApplicationContext.getBean(StudentController.class);
        bean.queryAll();




    }
}
