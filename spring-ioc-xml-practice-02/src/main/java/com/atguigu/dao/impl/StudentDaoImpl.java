package com.atguigu.dao.impl;

import com.atguigu.dao.StudentDao;
import com.atguigu.pojo.Student;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class StudentDaoImpl implements StudentDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * queryAll（） 这个方法由 jdbcTemplate 调用，进行数据库的查询，而 jdbcTemplate 由 ioc 容器装配，不要用程序员自己实例化的方式
     * 所以搞一个 JdbcTemplate 类的成员，和 setter
     *
     * @return
     */
    @Override
    public List<Student> queryAll() {
        String sql = "select id,name,gender,age,class as classes from students;";
        List<Student> studentList = jdbcTemplate.query(sql,new BeanPropertyRowMapper<>(Student.class));
        System.out.println("studentDao"+studentList);
        return studentList;
    }
}
