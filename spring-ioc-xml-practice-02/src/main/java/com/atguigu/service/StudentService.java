package com.atguigu.service;

import com.atguigu.dao.StudentDao;
import com.atguigu.pojo.Student;

import java.util.List;

public interface StudentService {
    List<Student> findAll();

}
