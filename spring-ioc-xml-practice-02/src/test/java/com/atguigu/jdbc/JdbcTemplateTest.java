package com.atguigu.jdbc;

import com.alibaba.druid.pool.DruidDataSource;
import com.atguigu.controller.StudentController;
import com.atguigu.pojo.Student;
import org.junit.jupiter.api.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * 测试 jdbcTemplate 如何使用
 */
public class JdbcTemplateTest {

    //通过 Java 代码实例化对象
    public void testForJdbc() {


        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setUrl("jdbc:mysql://localhost:3306/studb?serverTimezone=UTC&amp;characterEncoding=utf8&amp;useUnicode=true&amp;useSSL=false");
        druidDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        druidDataSource.setUsername("root");
        druidDataSource.setPassword("123456");

        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        // jdbcTemplate.update();
        jdbcTemplate.setDataSource(druidDataSource);


    }

    @Test
    //通过IoC 读取配置文件中的 JdbcTemplate 组件
    public void testForIoC() {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring-01.xml");

        JdbcTemplate jdbcTemplate = classPathXmlApplicationContext.getBean(JdbcTemplate.class);


        String sql = "insert into students (id,name,gender,age,class) values (?,?,?,?,?);";
        int row = jdbcTemplate.update(sql, 10,"Jerry", "male", "15", "美国大农村");
        System.out.println(row);

        sql = "select * from students where id = ?;";
       Student student1 = jdbcTemplate.queryForObject(sql, (rs,  rowNum) ->  {
                Student student = new Student();
                student.setId(rs.getInt("id"));
                student.setName(rs.getString("name"));
                student.setGender(rs.getString("gender"));
                student.setAge(rs.getInt("age"));
                student.setClasses(rs.getString("class"));

                return student;
            }, 3);
        System.out.println("student1 :"+student1);


        sql ="select id,name,gender,age,class as classes from students;";
        List<Student> studentList = jdbcTemplate.query(sql, new BeanPropertyRowMapper<Student>(Student.class));
        for (Student student : studentList) {
            System.out.println(student);
        }


    }



    @Test
    public void testQueryAll(){
        //1.初始化 ioc 容器
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring-02.xml");

        //2.获取组件对象，本例中就是获取 controller 实现类
        StudentController studentController = classPathXmlApplicationContext.getBean(StudentController.class);

        //3.使用组件
        studentController.findAll();

        //4.关闭ioc 容器
        classPathXmlApplicationContext.close();
    }





}
