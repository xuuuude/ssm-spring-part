package com.atguigu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TopService {
    @Autowired
    private StudentService studentService;

    public void topService(){
        studentService.changeAge();
        studentService.changeName();
    }


}
