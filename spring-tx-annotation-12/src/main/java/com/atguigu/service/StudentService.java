package com.atguigu.service;

import com.alibaba.druid.support.http.WebStatFilter;
import com.atguigu.dao.StudentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

@Transactional(isolation = Isolation.READ_COMMITTED)
@Service
public class StudentService {
    
    @Autowired
    private StudentDao studentDao;


    @Transactional(rollbackFor = Exception.class)
    public void changeInfo() throws FileNotFoundException {
        studentDao.updateAgeById(75,1);
        //FileInputStream r = new FileInputStream("abc");
        studentDao.updateNameById("test1",1);
    }

    @Transactional(readOnly = true)
    public void getStudentInfo(){
        //通常情况下，在类的上方添加注解进行添加事务，在查询方法的上方添加 readOnly True 来标注此方法在事务中是只读模式
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void changeAge(){
        studentDao.updateAgeById(998,1);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void changeName(){
        studentDao.updateNameById("TOM",1);
    }



}
