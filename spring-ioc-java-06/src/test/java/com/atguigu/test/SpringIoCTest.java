package com.atguigu.test;

import com.atguigu.config.JavaConfigA;
import com.atguigu.config.JavaConfigB;
import com.atguigu.config.JavaConfiguration;
import com.atguigu.ioc_01.StudentController;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringIoCTest {

    @Test
    public void Test(){
        //1.创建容器
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(JavaConfiguration.class);


        //2.根据配置类获取 bean
        StudentController bean = applicationContext.getBean(StudentController.class);
        System.out.println(bean);

    }


    @Test
    public void Test04(){
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(JavaConfigA.class);

    }
}
