package com.atguigu.config;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import(value ={ JavaConfigB.class} )
@Configuration
public class JavaConfigA {
}
