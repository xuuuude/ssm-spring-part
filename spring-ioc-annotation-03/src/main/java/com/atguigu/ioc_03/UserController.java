package com.atguigu.ioc_03;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

@Controller
public class UserController {


    @Autowired //自动装配，对属性进行 DI 操作，
    @Qualifier(value = "userServiceImpl")
    private UserService userService;

//    public void setUserService(UserService userService) {
//        this.userService = userService;
//    }
    public void show(){
        String show = userService.show();
        System.out.println(show);

    }

}
