package com.atguigu.ioc_03;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Override
    public String show() {
        System.out.println("Service 层：");
        return "UserServiceImpl show()" ;
    }
}
