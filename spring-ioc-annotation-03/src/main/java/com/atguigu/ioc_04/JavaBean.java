package com.atguigu.ioc_04;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class JavaBean {

    private String name = "Tom";

    @Value("20")
    private int age;



    @Value("${jdbc.username:root}")
    private String username;

    @Value("${jdbc.password:123456}")
    private String password;


    @Override
    public String toString() {
        return "JavaBean{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
