package com.atguigu.test;

import com.atguigu.ioc_01.XxxController;
import com.atguigu.ioc_01.XxxDao;
import com.atguigu.ioc_01.XxxService;
import com.atguigu.ioc_02.JavaBean;
import com.atguigu.ioc_03.UserController;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringIoCTest {

    @Test
    public void testIoc_01(){
        //1.创建 ioc 容器
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring-01.xml");

        //2.获取组件
        XxxDao bean = classPathXmlApplicationContext.getBean(XxxDao.class);

        System.out.println("XxxDao: "+bean);

        Object xxxService = classPathXmlApplicationContext.getBean("xxxService");
        System.out.println("xxxService: "+xxxService);

        //3.close容器
        classPathXmlApplicationContext.close();
    }


    @Test
    public void testIoc_02(){
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring-02.xml");

        JavaBean bean = classPathXmlApplicationContext.getBean(JavaBean.class);
        JavaBean bean1 = classPathXmlApplicationContext.getBean(JavaBean.class);

        System.out.println(bean == bean1);

        classPathXmlApplicationContext.close();
    }

    @Test
    public void testIoc_03(){
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring-03.xml");
        UserController userController = classPathXmlApplicationContext.getBean(UserController.class);
        userController.show();
        classPathXmlApplicationContext.close();
    }


    @Test
    public void testIoc_04(){
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring-04.xml");
        com.atguigu.ioc_04.JavaBean javaBean = classPathXmlApplicationContext.getBean(com.atguigu.ioc_04.JavaBean.class);
        System.out.println(javaBean);
        classPathXmlApplicationContext.close();
    }




}
