package com.atguigu;

import com.atguigu.dyn.ProxyFactory;
import com.atguigu.statics.StaticProxyCalculator;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UseAOP {

    public static void main(String[] args) {
        CalculatorPureImpl calculatorPure = new CalculatorPureImpl();

        StaticProxyCalculator staticProxyCalculator = new StaticProxyCalculator(calculatorPure);

        System.out.println(staticProxyCalculator.add(4,6));


        //JDK 代理
        ProxyFactory proxyFactory = new ProxyFactory(calculatorPure);
        Calculator proxy = (Calculator) proxyFactory.getProxy();
        proxy.add(2,5);


        /**
         * 代理，JDK 和 CGlib 的区别
         * JDK 的时候，接代理类对象的时候，要用被实现的接口类型去接，这里的代理类和 TargetCLass 实现了同一个接口
         * CGlib 的时候，要用被代理对象 TargetClass 类型去接，因为这里的代理类是  TargetClass 的子类
         */





    }

}
