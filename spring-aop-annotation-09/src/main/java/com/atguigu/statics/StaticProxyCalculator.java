package com.atguigu.statics;

import com.atguigu.Calculator;

public class StaticProxyCalculator implements Calculator {

    private Calculator calculator;

    //使用构造器将目标实例（核心行为类的实例）传入，将 target 赋值给 calculator 属性，也就是将目标注入静态代理类
    public StaticProxyCalculator(Calculator target){
        this.calculator = target;
    }

    @Override
    public int add(int i, int j) {
        System.out.println("i = " + i + ", j = " + j);
       int result = calculator.add(i,j);
       return result;

    }

    @Override
    public int sub(int i, int j) {
        return 0;
    }

    @Override
    public int mul(int i, int j) {
        return 0;
    }

    @Override
    public int div(int i, int j) {
        return 0;
    }
}
